/**
 * Copyright © Redbox. All rights reserved.
 */

'use strict';

module.exports = {
    en_US: {
        area: 'frontend',
        name: 'Beside/default',
        locale: 'en_US',
        files: [
            'css/styles-m',
            'css/styles-l'
        ],
        dsl: 'less'
    },
    ar_SA: {
        area: 'frontend',
        name: 'Beside/arabic',
        locale: 'ar_SA',
        files: [
            'css/styles-m',
            'css/styles-l'
        ],
        dsl: 'less'
    }
};
